import 'package:equatable/equatable.dart';
import 'package:flutter_comics_app/features/search/domain/entities/entities_search.dart';

class DataSearchEntity extends Equatable {
  const DataSearchEntity({
    required this.offset,
    required this.limit,
    required this.total,
    required this.count,
    required this.results,
  });

  final int offset;
  final int limit;
  final int total;
  final int count;
  final List<CharacterSearchEntity> results;

  @override
  List<Object?> get props => [
        offset,
        limit,
        total,
        count,
        results,
      ];
}
