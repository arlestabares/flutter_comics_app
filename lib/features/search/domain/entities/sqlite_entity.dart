import 'package:equatable/equatable.dart';
import 'package:flutter_comics_app/core/app/data/datasources/local/dto/character_search_picture.dart';

class SqliteEntity extends Equatable {
  const SqliteEntity({
    this.id,
    this.name,
    this.description,
    this.thumbnail,
    this.comics,
    this.series,
    this.stories,
    this.events,
  });

  final int? id;
  final String? name;
  final String? description;
  final String? thumbnail;
  final List<String>? comics;
  final List<String>? series;
  final List<String>? stories;
  final List<String>? events;

  factory SqliteEntity.fromDtoToEntity(CharacterSearchDTO dto) {
    return SqliteEntity(
      id: dto.id ?? 0,
      name: dto.name ?? '',
      description: dto.description ?? '',
      thumbnail: dto.thumbnail ?? '',
      comics: dto.comics ?? [],
      series: dto.series ?? [],
      stories: dto.stories ?? [],
      events: dto.events ?? [],
    );
  }
  @override
  List<Object?> get props => [
        id,
        name,
        description,
        thumbnail,
        comics,
        series,
        stories,
        events,
      ];
}
