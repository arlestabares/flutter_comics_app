import 'dart:typed_data';

import 'package:equatable/equatable.dart';

import '../../../../core/app/data/models/models.dart';

class CharacterSearchEntity extends Equatable {
  const CharacterSearchEntity({
    required this.id,
    required this.name,
    required this.description,
    required this.thumbnail,
    this.uint8list,
    required this.comics,
    required this.series,
    required this.stories,
    required this.events,
    this.picture
  });

  final int id;
  final String name;

  final String description;
  final Thumbnail? thumbnail;
  final Uint8List? uint8list;
  final List<String> comics;
  final List<String> series;
  final List<String> stories;
  final List<String> events;
  final String? picture;

 
  @override
  List<Object?> get props => [
        id,
        name,
        description,
        thumbnail,
        comics,
        series,
        stories,
        events,
      ];
}
