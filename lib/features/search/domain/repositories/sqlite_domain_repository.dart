import 'package:flutter_comics_app/features/search/domain/entities/sqlite_entity.dart';

abstract class SqliteDomainRepository {
  Future<int> insertCharacter(SqliteEntity characterPictureEntity);
  Future<List<int>> insertCharacterList(
      List<SqliteEntity> characterPictureEntityList);
  Future<List<SqliteEntity>> getAllCharacter();
  Future<void> deleteCharacter(SqliteEntity characterPictureEntity);

  Future init();
  void close();
}
