import 'package:dartz/dartz.dart';
import 'package:flutter_comics_app/features/search/domain/entities/entities_search.dart';

import '../../../../core/errors/failure.dart';

abstract class CharacterSearchDomainRepository {
  Future<Either<Failure, DataSearchEntity>> getCharacterByName(
      String nameCharacter, int offset);
  Future<Either<Failure, DataSearchEntity>> getFavoritesCharacterDB();
  Future<Either<Failure, DataSearchEntity>> getCharacterDB();
}
