import 'package:flutter_comics_app/features/search/domain/entities/sqlite_entity.dart';
import 'package:flutter_comics_app/features/search/domain/repositories/sqlite_domain_repository.dart';

class InsertCharacterDbUseCase {
  final SqliteDomainRepository sqliteDomainRepository;

  InsertCharacterDbUseCase(this.sqliteDomainRepository);

  Future<int> call(SqliteEntity characterPictureEntity) {
    return sqliteDomainRepository.insertCharacter(characterPictureEntity);
  }
}
