import 'package:equatable/equatable.dart';
import 'package:flutter_comics_app/core/errors/failure.dart';
import 'package:dartz/dartz.dart';
import 'package:flutter_comics_app/core/usecases/use_case.dart';
import 'package:flutter_comics_app/features/search/domain/entities/entities_search.dart';
import 'package:flutter_comics_app/features/search/domain/repositories/character_search_repository.dart';
// import 'package:flutter_comics_app/features/domain/usecases/use_cases.dart';

class GetSearchCharacterDbUseCase implements IUseCaseCore<DataSearchEntity, ParamsOffset> {
  final CharacterSearchDomainRepository comicsRepository;

  GetSearchCharacterDbUseCase(this.comicsRepository);
   @override
  Future<Either<Failure, DataSearchEntity>> call(ParamsOffset params) async {
    return await comicsRepository.getCharacterDB();
  }
}

class ParamsOffset extends Equatable{
  final int offset;

  const ParamsOffset(this.offset);
  @override
  List<Object?> get props => [];

}
