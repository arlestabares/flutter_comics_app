import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter_comics_app/core/errors/failure.dart';
import 'package:flutter_comics_app/core/usecases/use_case.dart';
import 'package:flutter_comics_app/features/search/domain/entities/entities_search.dart';
import 'package:flutter_comics_app/features/search/domain/repositories/character_search_repository.dart';


class GetSearchCharacterByNameUseCase implements IUseCaseCore<DataSearchEntity, ParamsByNameAndOffset> {
  final CharacterSearchDomainRepository? domainRepository;
  GetSearchCharacterByNameUseCase(this.domainRepository);

  @override
  Future<Either<Failure, DataSearchEntity>> call(ParamsByNameAndOffset params) async {
    return await domainRepository!
        .getCharacterByName(params.nameCharacter, params.offset);
  }
}
class ParamsByNameAndOffset extends Equatable{
  final String nameCharacter;
  final int offset;

  const ParamsByNameAndOffset(this.nameCharacter, this.offset);
  @override
  List<Object?> get props => [nameCharacter, offset];

}

