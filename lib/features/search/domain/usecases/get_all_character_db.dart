import 'package:flutter_comics_app/features/search/domain/entities/sqlite_entity.dart';
import 'package:flutter_comics_app/features/search/domain/repositories/sqlite_domain_repository.dart';

class GetAllCharacterDbUseCase {
  final SqliteDomainRepository sqliteDomainRepository;

  GetAllCharacterDbUseCase(this.sqliteDomainRepository);

  Future<List<SqliteEntity>> call() {
    return sqliteDomainRepository.getAllCharacter();
  }
}
