part of 'search_bloc.dart';

abstract class SearchState extends Equatable {
  const SearchState(this.model);
  final Model model;

  @override
  List<Object> get props => [];
}

class SearchInitialState extends SearchState {
  const SearchInitialState(Model model) : super(model);
}

class SearchCharacterByNameUploadedState extends SearchState {
  const SearchCharacterByNameUploadedState(Model model) : super(model);
}

class SearchCharacterByNameErrorUploadState extends SearchState {
  const SearchCharacterByNameErrorUploadState(Model model) : super(model);
}

class FavoriteLoadedFromDbState extends SearchState {
  const FavoriteLoadedFromDbState(Model model) : super(model);
}

class AddedFavoriteToDbState extends SearchState {
  const AddedFavoriteToDbState(Model model) : super(model);
}

class CharactersUploadedState extends SearchState {
  const CharactersUploadedState(Model model) : super(model);
}

class CharactersUploadErrorState extends SearchState {
  const CharactersUploadErrorState(Model model) : super(model);
}

class CharacterFavoriteAddedState extends SearchState {
  const CharacterFavoriteAddedState(Model model) : super(model);
}

class Model extends Equatable {
  final List<CharacterSearchEntity> characterSearchList;
  final List<SqliteEntity> characterFavoritesList;
  final List<SqliteEntity> characterFromDBList;
  final CharacterSearchStatus status;
  final bool hasReachedMax;
  final String query;
  const Model({
    this.query = '',
    this.hasReachedMax = false,
    this.status = CharacterSearchStatus.initial,
    this.characterFromDBList = const <SqliteEntity>[],
    this.characterFavoritesList = const <SqliteEntity>[],
    this.characterSearchList = const <CharacterSearchEntity>[],
  });

  Model copyWith({
    String? query,
    bool? hasReachedMax,
    CharacterSearchStatus? status,
    List<SqliteEntity>? characterFavoritesList,
    List<SqliteEntity>? characterFromDBList,
    List<CharacterSearchEntity>? characterSearchList,
  }) =>
      Model(
        query: query ?? this.query,
        status: status ?? this.status,
        hasReachedMax: hasReachedMax ?? this.hasReachedMax,
        characterFavoritesList:
            characterFavoritesList ?? this.characterFavoritesList,
        characterFromDBList: characterFromDBList ?? this.characterFromDBList,
        characterSearchList: characterSearchList ?? this.characterSearchList,
      );

  @override
  List<Object?> get props => [
        status,
        characterSearchList,
        hasReachedMax,
        query,
        characterFavoritesList
      ];
}

enum CharacterSearchStatus { initial, success, failure }
