import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter_comics_app/features/search/domain/entities/entities_search.dart';
import 'package:flutter_comics_app/features/search/domain/entities/sqlite_entity.dart';
import 'package:flutter_comics_app/features/search/domain/usecases/get_all_character_db.dart';
import 'package:flutter_comics_app/features/search/domain/usecases/insert_character_db.dart';
import 'package:flutter_comics_app/features/search/domain/usecases/use_cases.dart';

part 'search_event.dart';
part 'search_state.dart';

class SearchBloc extends Bloc<SearchEvent, SearchState> {
  final GetSearchCharacterDbUseCase getSearchCharacterDbUseCase;
  final GetSearchCharacterByNameUseCase getSearchCharacterByNameUseCase;
  final InsertCharacterDbUseCase insertCharacterDbUseCase;
  final GetAllCharacterDbUseCase getAllCharacterDbUseCase;
  SearchBloc({
    required GetAllCharacterDbUseCase findAllCharacterDb,
    required InsertCharacterDbUseCase insertCharacterSearchDbUseCase,
    required GetSearchCharacterDbUseCase searchCharacterDbUseCase,
    required GetSearchCharacterByNameUseCase searchCharacterByNameUseCase,
  })  : getSearchCharacterDbUseCase = searchCharacterDbUseCase,
        getSearchCharacterByNameUseCase = searchCharacterByNameUseCase,
        insertCharacterDbUseCase = insertCharacterSearchDbUseCase,
        getAllCharacterDbUseCase = findAllCharacterDb,
        super(initialState) {
    on<CharacterSearchByNameEvent>(_onSearchCharacterEvent);
    on<AddFavoritesEvent>(_onAddFavoritesEvent);
    on<AddFavoriteToDbEvent>(_onAddFavoriteToDbEvent);
    on<GetFavoritesFromDbEvent>(_onGetFavoritesFromDbEvent);
  }
  static SearchState get initialState => const SearchInitialState(Model());

  _onSearchCharacterEvent(
      CharacterSearchByNameEvent event, Emitter<SearchState> emit) async {
    if (state.model.hasReachedMax) return;
    state.model.query != event.nameCharacter;
    var currentCharactersContent = <CharacterSearchEntity>[];
    if (state is SearchCharacterByNameUploadedState) {
      currentCharactersContent = state.model.characterSearchList;
    }
    final result =
        await getSearchCharacterByNameUseCase.call(ParamsByNameAndOffset(
      event.nameCharacter,
      state.model.characterSearchList.length,
    ));

    result.fold(
      (failure) => emit(SearchCharacterByNameErrorUploadState(
        state.model.copyWith(status: CharacterSearchStatus.failure),
      )),
      (data) {
        if (data.results.isEmpty) {
          emit(SearchCharacterByNameUploadedState(
            state.model.copyWith(
              status: CharacterSearchStatus.success,
              hasReachedMax: true,
            ),
          ));
        } else {
          emit(
            SearchCharacterByNameUploadedState(
              state.model.copyWith(
                status: CharacterSearchStatus.success,
                characterSearchList: currentCharactersContent
                  ..addAll(data.results),
                hasReachedMax: false,
                query: event.nameCharacter,
              ),
            ),
          );
        }
      },
    );
  }

  _onAddFavoritesEvent(
      AddFavoritesEvent event, Emitter<SearchState> emit) async {
    var favoritesContent = <SqliteEntity>[];
    if (state.model.characterFavoritesList.contains(event.favorites)) {
      return;
    } else {
      emit(
        CharacterFavoriteAddedState(
          state.model.copyWith(
              characterFavoritesList:
                  List.of(state.model.characterFavoritesList)
                    ..add(event.favorites)),
        ),
      );
    }

    if (state is CharacterFavoriteAddedState) {
      favoritesContent = state.model.characterFavoritesList;
    }
    print(event.favorites.name);
  }

  _onAddFavoriteToDbEvent(
      AddFavoriteToDbEvent event, Emitter<SearchState> emit) async {
    final insert =
        await insertCharacterDbUseCase.call(event.characterSearchEntity);
    print('Valor de la list  db====$insert');
    emit(AddedFavoriteToDbState(state.model));
  }

  _onGetFavoritesFromDbEvent(
      GetFavoritesFromDbEvent event, Emitter<SearchState> emit) async {
    final getAllCharacterDb = await getAllCharacterDbUseCase.call();
    emit(FavoriteLoadedFromDbState(
        state.model.copyWith(characterFromDBList: getAllCharacterDb)));
    String n = '';
    String t = '';
    for (var element in getAllCharacterDb) {
      n = element.name!;
      t = element.description!;
    }
    print(n);
    print(t);
  }
}
