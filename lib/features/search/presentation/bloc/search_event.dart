part of 'search_bloc.dart';

abstract class SearchEvent extends Equatable {
  const SearchEvent();

  @override
  List<Object> get props => [];
}

class CharacterSearchEvent extends SearchEvent {}

class CharacterSearchByNameEvent extends SearchEvent {
  final String nameCharacter;

  const CharacterSearchByNameEvent(this.nameCharacter);
}

class CharactersDbUploadedSearchEvent extends SearchEvent {}

class AddFavoriteToDbEvent extends SearchEvent {
  final SqliteEntity characterSearchEntity;

  const AddFavoriteToDbEvent(this.characterSearchEntity);
}

class AddFavoritesEvent extends SearchEvent {
  final SqliteEntity favorites;

  const AddFavoritesEvent(this.favorites);
}

class GetFavoritesFromDbEvent extends SearchEvent {}
