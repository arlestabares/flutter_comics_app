import 'package:flutter/material.dart';
import 'package:flutter_comics_app/features/search/presentation/views/favorites/favorites_list.dart';

class FavoritesPage extends StatelessWidget {
  const FavoritesPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Favorites'),
      ),
      body: const FavoritesList(),
    );
  }
}
