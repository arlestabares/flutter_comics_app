import 'package:flutter/material.dart';
import 'package:flutter_comics_app/utils/const.dart';

class TextFieldContainer extends StatelessWidget {
  final String? hintText;
  final Widget? icon;
  final Widget? suffixIcon;
  final bool obscureText;
  final Function(String)? onChange;
  final Function()? onTap;
  final Function(String)? onSubmitted;
  final Function()? onEditingComplete;
  final TextEditingController? controller;
  final bool? enableInteractiveSelection;

  //
  const TextFieldContainer({
    Key? key,
    this.icon,
    this.onTap,
    this.hintText,
    this.controller,
    this.suffixIcon,
    this.onSubmitted,
    this.obscureText = true,
    this.onEditingComplete,
    required this.onChange,
    this.enableInteractiveSelection,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.only(left: 16.0, right: 16.0, bottom: 21.0),
      child: TextField(
        onTap: onTap,
        onSubmitted: onSubmitted,
        enableInteractiveSelection: enableInteractiveSelection,
        onEditingComplete: onEditingComplete,
        controller: controller,
        obscureText: obscureText,
        decoration: InputDecoration(
          focusedBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(21.0),
            borderSide: const BorderSide(color: bgColor),
          ),
          hintText: hintText,
          border: OutlineInputBorder(
            borderSide: const BorderSide(color: bgColor),
            borderRadius: BorderRadius.circular(21.0),
          ),
          prefixIcon: icon,
          suffixIcon: suffixIcon,
        ),
        onChanged: onChange,
      ),
    );
  }
}
