import 'package:flutter/material.dart';

AppBar appbarSearchWidget(BuildContext context) {
  return AppBar(
    elevation: 0.0,
    backgroundColor: Colors.white,
    leading: IconButton(
      onPressed: () {
        Navigator.pop(context);
      },
      icon: const Icon(
        Icons.arrow_back_ios,
        color: Colors.black,
      ),
    ),
    actions: [
      IconButton(
        icon: const Icon(Icons.favorite,color: Colors.red,),
        onPressed: () {

        },
      ),
    ],
  );
}
