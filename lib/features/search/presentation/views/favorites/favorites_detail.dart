import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_comics_app/features/home/presentation/widgets/hero_widget.dart';
import 'package:flutter_comics_app/features/search/domain/entities/sqlite_entity.dart';
import 'package:flutter_comics_app/features/search/presentation/bloc/search_bloc.dart';
import 'package:flutter_comics_app/features/search/presentation/views/search_delegate/detail/detail.dart';

class FavoritesDetail extends StatelessWidget {
  const FavoritesDetail({
    Key? key,
     this.character,
    this.comicList,
  }) : super(key: key);
  final SqliteEntity? character;
  final List<String>? comicList;
  @override
  Widget build(BuildContext context) {
    String _base64 = '';
    _base64 = character!.thumbnail!;

    // var bytes = base64Encode(_base64) ;

    // var thumbnail = bytes;

    var sqliteEntity = SqliteEntity(
      id: character!.id,
      name: character!.name ?? '',
      description: character!.description ?? '',
      thumbnail: 'lola',
      comics: character!.comics ?? [],
      series: character!.series ?? [],
      stories: character!.stories ?? [],
      events: character!.events ?? [],
    );

    final size = MediaQuery.of(context).size;
    return Scaffold(
      appBar: AppBar(
        elevation: 0.0,
        backgroundColor: Colors.white,
        leading: IconButton(
          onPressed: () async {
            Navigator.pop(context);
          },
          icon: const Icon(
            Icons.arrow_back_ios,
            color: Colors.black,
          ),
        ),
        actions: [
          IconButton(
            icon: const Icon(
              Icons.favorite,
              color: Colors.red,
            ),
            onPressed: () {
              context
                  .read<SearchBloc>()
                  .add(AddFavoriteToDbEvent(sqliteEntity));
            },
          ),
        ],
      ),
      body: ListView(
        children: [
          Stack(
            children: [
              Container(
                height: size.height * 0.3,
                width: size.width,
                // color: Colors.black38,
                padding: const EdgeInsets.all(5.0),
                child: HeroWidget(
                  tag: character!.name,
                  radius: 21.0,
                  assetImage: 'assets/images/no-image.jpg',
                  networkImage: character!.thumbnail,
                  fit: BoxFit.fill,
                ),
              ),
              Positioned(
                left: 12.0,
                top: 6.0,
                child: Text(
                  character!.name!,
                  style: const TextStyle(
                      fontSize: 21.0, fontWeight: FontWeight.bold),
                ),
              ),
            ],
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 12.0),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Expanded(
                  child: Text(
                    character!.description!.isEmpty
                        ? 'No description......'
                        : character!.description!,
                    style: TextStyle(
                        fontSize: character!.description!.isEmpty ? 21.0 : 16.0,
                        fontWeight: FontWeight.bold),
                  ),
                ),
              ],
            ),
          ),
          const SizedBox(height: 8.0),
          const Padding(
            padding: EdgeInsets.symmetric(horizontal: 8.0),
            child: Text(
              'Lista de Comics asociados',
              style: TextStyle(fontSize: 18.0),
            ),
          ),
          CharacterComicsList(comicList: comicList!)
        ],
      ),
    );
  }
}
