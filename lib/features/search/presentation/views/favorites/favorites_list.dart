import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_comics_app/features/home/presentation/widgets/size_transition.dart';
import 'package:flutter_comics_app/features/search/presentation/bloc/search_bloc.dart';
import 'package:flutter_comics_app/features/search/presentation/views/favorites/favorites_detail.dart';
import 'package:flutter_comics_app/features/search/presentation/views/favorites/favorites_card.dart';

class FavoritesList extends StatelessWidget {
  const FavoritesList({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<SearchBloc, SearchState>(
      builder: (context, state) {
        if (state.model.characterFromDBList.isEmpty) {
          return const Center(
            key: Key('No Character'),
            child: Card(
              child: ListTile(
                title: Text('There are not character favorites to show'),
              ),
            ),
          );
        }
        return ListView.builder(
          shrinkWrap: true,
          itemCount: state.model.characterFromDBList.length,
          itemBuilder: (context, int index) {
            return InkWell(
              onTap: () => Navigator.push(
                context,
                SizeTransitionRoute(
                  page: FavoritesDetail(
                    character: state.model.characterFromDBList[index],
                    comicList: state.model.characterFromDBList[index].comics!,
                  ),
                ),
              ),
              child: FavoritesCard(
                character: state.model.characterFromDBList[index],
              ),
            );
          },
        );
      },
    );
  }
}
