import 'package:flutter/material.dart';
import 'package:flutter_comics_app/features/search/domain/entities/entities_search.dart';

class SearchDelegateCard extends StatelessWidget {
  final CharacterSearchEntity character;

  const SearchDelegateCard({Key? key, required this.character})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: const BoxDecoration(
        boxShadow: [
          BoxShadow(
            color: Colors.black12,
            blurRadius: 15.0,
            offset: Offset(-3.0, 10.0),
          )
        ],
        shape: BoxShape.rectangle,
        color: Colors.transparent,
      ),
      margin: const EdgeInsets.fromLTRB(20, 8, 20, 10),
      child: ClipRRect(
        borderRadius: const BorderRadius.only(
          topLeft: Radius.circular(30.0),
          bottomLeft: Radius.circular(30.0),
        ),
        child: Container(
          color: Colors.white,
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Hero(
                tag: character.id.toString(),
                child: Image.network(
                  '${character.thumbnail!.path}.${character.thumbnail!.extension}',
                  width: 150,
                  height: 160,
                  fit: BoxFit.fill,
                ),
              ),
              Flexible(
                child: Padding(
                  padding: const EdgeInsets.all(10.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        character.name,
                        style: const TextStyle(fontSize: 20.0),
                      ),
                      Text(
                        character.description.isEmpty
                            ? 'No Description'
                            : character.description,
                        style: TextStyle(
                          fontWeight: FontWeight.normal,
                          fontSize:
                              (character.description.isEmpty) ? 21.0 : 16.0,
                        ),
                        maxLines: 2,
                        overflow: TextOverflow.ellipsis,
                        softWrap: false,
                      ),
                    ],
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
