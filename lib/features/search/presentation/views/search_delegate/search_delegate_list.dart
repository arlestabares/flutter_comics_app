import 'dart:async';

import 'package:animations/animations.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_comics_app/features/home/presentation/widgets/bottom_loading.dart';
import 'package:flutter_comics_app/features/home/presentation/widgets/size_transition.dart';
import 'package:flutter_comics_app/features/search/presentation/bloc/search_bloc.dart';
import 'package:flutter_comics_app/features/search/presentation/views/search_delegate/detail/search_delegate_card.dart';
import 'package:flutter_comics_app/features/search/presentation/views/search_delegate/detail/search_delegate_details.dart';

class CharactersSearchDelegateList extends StatefulWidget {
  const CharactersSearchDelegateList({Key? key, this.query}) : super(key: key);
  final String? query;

  @override
  State<CharactersSearchDelegateList> createState() =>
      _CharactersSearchDelegateListState();
}

class _CharactersSearchDelegateListState
    extends State<CharactersSearchDelegateList> {
  ContainerTransitionType transitionType = ContainerTransitionType.fade;
  final _scrollController = ScrollController();

  @override
  void initState() {
    super.initState();
    _scrollController.addListener(_scrollListener);
  }

  @override
  void dispose() {
    _scrollController
      ..removeListener(_scrollListener)
      ..dispose();
    super.dispose();
  }

  _scrollListener() {
    if (_scrollController.offset >=
            _scrollController.position.maxScrollExtent &&
        !_scrollController.position.outOfRange) {
      setState(() {
        context
            .read<SearchBloc>()
            .add(CharacterSearchByNameEvent(widget.query!));
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<SearchBloc, SearchState>(
      builder: (context, state) {
        if (state is SearchCharacterByNameUploadedState) {
          switch (state.model.status) {
            case CharacterSearchStatus.success:
              if (state.model.characterSearchList.isEmpty) {
                return const Center(
                  key: Key('No Character'),
                  child: Card(
                    child: ListTile(
                      title: Text('There are not character to show'),
                    ),
                  ),
                );
              }
              final itemCount = state.model.hasReachedMax
                  ? state.model.characterSearchList.length
                  : state.model.characterSearchList.length + 1;
              return widget.query!.isNotEmpty
                  ? ListView.builder(
                      controller: _scrollController,
                      itemCount: itemCount,
                      itemBuilder: (context, int index) {
                        if (index < (state.model.characterSearchList.length)) {
                          return InkWell(
                            onTap: () => Navigator.push(
                              context,
                              SizeTransitionRoute(
                                page: SearchDelegateDetails(
                                  character:
                                      state.model.characterSearchList[index],
                                  comicList: state
                                      .model.characterSearchList[index].comics,
                                ),
                              ),
                            ),
                            child: SearchDelegateCard(
                              character: state.model.characterSearchList[index],
                            ),
                          );
                          // OpenContainerWrapper(
                          //   transitionType: transitionType,
                          //   openBuilder: (context, closedOpenContainer) =>
                          //     OpenContainerCharacterDetails(
                          //   character: state.model.characterSearchList[index],
                          //   path: state.model.characterSearchList[index].thumbnail.path!,
                          //   extension: state
                          //       .model.characterSearchList[index].thumbnail.extension!,
                          //   comicList: state.model.characterSearchList[index].comics,
                          // ),
                          //   closedBuilder: (context, openContainer) => CharacterCard(
                          //     character: state.model.characterSearchList[index],
                          //   ),
                          // );
                        } else {
                          Timer(const Duration(milliseconds: 10), () {
                            _scrollController.jumpTo(
                                _scrollController.position.maxScrollExtent);
                          });
                          return const Center(
                            child: SizedBox(
                              width: 70,
                              height: 70,
                              child: BottomLoading(),
                            ),
                          );
                        }
                      },
                    )
                  : const SizedBox.shrink();
            case CharacterSearchStatus.failure:
              return const Center(
                key: Key('failed_to_fetch_characters'),
                child: Card(
                  child: ListTile(
                    title: Text('failed to fetch characters'),
                  ),
                ),
              );
            default:
              return const SizedBox.shrink();
          }
        }
        return const SizedBox.shrink();
      },
    );
  }
}
