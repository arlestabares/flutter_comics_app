import 'dart:async';

import 'package:flutter_comics_app/core/app/data/datasources/local/dto/character_search_picture.dart';
import 'package:flutter_comics_app/features/search/domain/entities/sqlite_entity.dart';
import 'package:flutter_comics_app/features/search/domain/repositories/sqlite_domain_repository.dart';

import '../../../../core/app/data/datasources/local/sqlite/database_helper.dart';

class SqliteRepository extends SqliteDomainRepository {
  final dbHelper = DatabaseHelper.instance;

  @override
  Future<List<int>> insertCharacterList(
      List<SqliteEntity> characterPictureEntityList) {
    return Future(
      () async {
        if (characterPictureEntityList.isNotEmpty) {
          final characterIds = <int>[];
          await Future.forEach(characterPictureEntityList,
              (SqliteEntity character) async {
            final futureId = await dbHelper.insertCharacterSearch(
                CharacterSearchDTO.fromDomainToDto(character));
            // character.id = futureId;
            characterIds.add(futureId);
          });
          return Future.value(characterIds);
        } else {
          return Future.value(<int>[]);
        }
      },
    );
  }

  @override
  Future<void> deleteCharacter(SqliteEntity characterPictureEntity) {
    dbHelper.deleteCharacterSearch(
        CharacterSearchDTO.fromDomainToDto(characterPictureEntity));
    return Future.value();
  }

  @override
  Future<List<SqliteEntity>> getAllCharacter() async {
    var lista = dbHelper.findAllCharacterSearch();
    var tem = lista.then((value) =>
        value.map((e) => SqliteEntity.fromDtoToEntity(e)).toList());
    return tem;
  }

  @override
  Future<int> insertCharacter(SqliteEntity characterPictureEntity) {
    return dbHelper.insertCharacterSearch(
        CharacterSearchDTO.fromDomainToDto(characterPictureEntity));
  }

  @override
  Future init() async {
    await dbHelper.database;
    return Future.value();
  }

  @override
  void close() {
    dbHelper.close();
  }
}
