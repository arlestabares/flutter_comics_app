import 'dart:io';

import 'package:flutter_comics_app/core/errors/exceptions.dart';
import 'package:flutter_comics_app/core/errors/failure.dart';
import 'package:dartz/dartz.dart';
import 'package:flutter_comics_app/features/search/data/datasources/remote/remote_data_source.dart';
import 'package:flutter_comics_app/features/search/domain/entities/entities_search.dart';
import 'package:flutter_comics_app/features/search/domain/repositories/character_search_repository.dart';

class CharacterSearchRepostoryImpl implements CharacterSearchDomainRepository {
  final IRemoteSearchDataSource iRemoteDataSource;

  CharacterSearchRepostoryImpl(this.iRemoteDataSource);

  @override
  Future<Either<Failure, DataSearchEntity>> getCharacterByName(
      String nameCharacter, int offset) async {
    try {
      final data =
          await iRemoteDataSource.getCharacterByName(nameCharacter, offset);
      return Right(data.dataSearchModeltoEntity());
    } on ServerException {
      return const Left(ServerFailure('Failed the Server'));
    } on SocketException {
      return const Left(ConnectionFailure('Failed to connect to the network'));
    }
  }

  @override
  Future<Either<Failure, DataSearchEntity>> getCharacterDB() {
    throw UnimplementedError();
  }

  @override
  Future<Either<Failure, DataSearchEntity>> getFavoritesCharacterDB() {
    throw UnimplementedError();
  }
}
