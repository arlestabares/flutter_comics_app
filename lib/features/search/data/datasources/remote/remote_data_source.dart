import 'dart:convert';

import 'package:flutter_comics_app/core/errors/exceptions.dart';
import 'package:flutter_comics_app/core/network/urls_path.dart';
import 'package:http/http.dart' as http;

import '../../../../../core/app/data/models/models.dart';

abstract class IRemoteSearchDataSource {
  Future<DataModel> getCharacterByName(String nameCharacter, int offset);
}

class RemoteSearchDataSourceImpl implements IRemoteSearchDataSource {
  @override
  Future<DataModel> getCharacterByName(String nameCharacter, int offset) async {
    const limit = 10;
    final response = await http.get(Uri.parse(
        UrlPaths.searchCharactersByName(limit, nameCharacter, offset)));
    // final response = await http
    //     .get(Uri.parse(UrlPaths.characters(limit, nameCharacter, offset)));

    if (response.statusCode == 200) {
      final map = jsonDecode(response.body);
      return DataModel.fromJson(map['data']);
    } else {
      throw ServerException();
    }
  }
}
