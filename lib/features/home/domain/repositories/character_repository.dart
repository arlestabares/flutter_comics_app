import 'package:dartz/dartz.dart';
import 'package:flutter_comics_app/features/home/domain/entities/data.dart';
import 'package:flutter_comics_app/core/errors/failure.dart';

abstract class CharacterDomainRepository {
  Future<Either<Failure, Data>> getCharacters(int offset);
  Future<Either<Failure, Data>> getFavoritesCharacterDB();
  Future<Either<Failure, Data>> getCharacterDB();
}
