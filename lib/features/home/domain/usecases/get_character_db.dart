import 'package:flutter_comics_app/core/errors/failure.dart';
import 'package:dartz/dartz.dart';
import 'package:flutter_comics_app/core/usecases/use_case.dart';
import 'package:flutter_comics_app/features/home/domain/entities/entities.dart';
import 'package:flutter_comics_app/features/home/domain/repositories/character_repository.dart';

class GetCharacterDBUseCase implements IUseCaseCore<Data, NoParams> {
  final CharacterDomainRepository? domainRepository;

  GetCharacterDBUseCase(this.domainRepository);
  @override
  Future<Either<Failure, Data>> call(NoParams noParams) {
    throw UnimplementedError();
  }
}
