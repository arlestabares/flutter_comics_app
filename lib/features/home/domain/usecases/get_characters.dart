import 'package:equatable/equatable.dart';
import 'package:flutter_comics_app/core/errors/failure.dart';
import 'package:dartz/dartz.dart';
import 'package:flutter_comics_app/core/usecases/use_case.dart';
import 'package:flutter_comics_app/features/home/domain/entities/entities.dart';
import 'package:flutter_comics_app/features/home/domain/repositories/character_repository.dart';
// import 'package:flutter_comics_app/features/domain/usecases/use_cases.dart';

class GetCharacterUseCase implements IUseCaseCore<Data, ParamsOffset> {
  final CharacterDomainRepository comicsRepository;

  GetCharacterUseCase(this.comicsRepository);
   @override
  Future<Either<Failure, Data>> call(ParamsOffset params) async {
    return await comicsRepository.getCharacters(params.offset);
  }
}

class ParamsOffset extends Equatable{
  final int offset;

  const ParamsOffset(this.offset);
  @override
  List<Object?> get props => [];

}
