import 'package:dartz/dartz.dart';
import 'package:flutter_comics_app/features/home/domain/entities/data.dart';
import 'package:flutter_comics_app/features/home/domain/repositories/character_repository.dart';
import 'package:flutter_comics_app/core/errors/failure.dart';
import 'package:flutter_comics_app/core/usecases/use_case.dart';

class GetFavoritesCharacterUseCase implements IUseCaseCore<Data, NoParams> {
  final CharacterDomainRepository comicsRepository;

  GetFavoritesCharacterUseCase(this.comicsRepository);

  @override
  Future<Either<Failure, Data>> call(NoParams noParams) async {
    return await comicsRepository.getFavoritesCharacterDB();
  }
}
