import 'dart:io';

import 'package:flutter_comics_app/core/errors/exceptions.dart';
import 'package:flutter_comics_app/core/errors/failure.dart';
import 'package:dartz/dartz.dart';
import 'package:flutter_comics_app/features/home/data/datasources/datasource.dart';
import 'package:flutter_comics_app/features/home/domain/entities/data.dart';
import 'package:flutter_comics_app/features/home/domain/repositories/character_repository.dart';

class CharacterRepostoryImpl implements CharacterDomainRepository {
  final IRemoteDataSource iRemoteDataSource;

  CharacterRepostoryImpl(this.iRemoteDataSource);

  @override
  Future<Either<Failure, Data>> getCharacters(int offset) async {
    try {
      final data = await iRemoteDataSource.getCharacter(offset);
      return Right(data.dataModeltoEntity());
    } on ServerException {
      return const Left(ServerFailure('Failed the Server'));
    } on SocketException {
      return const Left(ConnectionFailure('Failed to connect to the network'));
    }
  }

  @override
  Future<Either<Failure, Data>> getCharacterDB() {
    throw UnimplementedError();
  }

  @override
  Future<Either<Failure, Data>> getFavoritesCharacterDB() {
    throw UnimplementedError();
  }
}
