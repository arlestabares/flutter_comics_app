import 'dart:convert';

import 'package:flutter_comics_app/core/errors/exceptions.dart';
import 'package:flutter_comics_app/core/network/urls_path.dart';
import 'package:http/http.dart' as http;

import '../../../../core/app/data/models/models.dart';

abstract class IRemoteDataSource {
  Future<DataModel> getCharacter(int offset);
}

class RemoteDataSourceImpl implements IRemoteDataSource {
  @override
  Future<DataModel> getCharacter(int offset) async {
    const limit = 10;
    // const offset = 0;
    final response =
        await http.get(Uri.parse(UrlPaths.searchCharacters(limit, offset)));
    if (response.statusCode == 200) {
      final map = jsonDecode(response.body);
      return DataModel.fromJson(map['data']);
    } else {
      throw ServerException();
    }
  }
}
