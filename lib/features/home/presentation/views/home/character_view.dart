import 'dart:async';

import 'package:animations/animations.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_comics_app/features/home/presentation/views/details/open_container_details.dart';
import 'package:flutter_comics_app/features/home/presentation/widgets/size_transition.dart';

import '../../bloc/character_bloc.dart';
import '../../widgets/bottom_loading.dart';
import '../../widgets/character_card.dart';

class CharactersView extends StatefulWidget {
  const CharactersView({Key? key}) : super(key: key);

  @override
  State<CharactersView> createState() => _CharactersViewState();
}

class _CharactersViewState extends State<CharactersView> {
  ContainerTransitionType transitionType = ContainerTransitionType.fade;
  late ScrollController _scrollController;

  @override
  void initState() {
    _scrollController = ScrollController()..addListener(_scrollListener);
    super.initState();
  }

  @override
  void dispose() {
    _scrollController
      ..removeListener(_scrollListener)
      ..dispose();
    super.dispose();
  }

  _scrollListener() {
    if (_scrollController.offset >=
            _scrollController.position.maxScrollExtent &&
        !_scrollController.position.outOfRange) {
      setState(() {
        context.read<CharacterBloc>().add(CharacterFetchedEvent());
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    ContainerTransitionType transitionType = ContainerTransitionType.fade;
    return BlocBuilder<CharacterBloc, CharacterState>(
      builder: (context, state) {
        if (state is CharactersUploadedState) {
          switch (state.model.status) {
            case CharacterStatus.success:
              if (state.model.characterList.isEmpty) {
                return const Center(
                  key: Key('No Character'),
                  child: Card(
                    child: ListTile(
                      title: Text('There are not character to show'),
                    ),
                  ),
                );
              }

              final itemCount = state.model.hasReachedMax
                  ? state.model.characterList.length
                  : state.model.characterList.length + 1;
              return ListView.builder(
                shrinkWrap: true,
                controller: _scrollController,
                itemCount: itemCount,
                itemBuilder: (context, int index) {
                  if (index < (state.model.characterList.length)) {
                    return InkWell(
                      onTap: () => Navigator.push(
                          context,
                          SizeTransitionRoute(
                            page: OpenContainerCharacterDetails(
                              character: state.model.characterList[index],
                              path: state
                                  .model.characterList[index].thumbnail.path!,
                              extension: state.model.characterList[index]
                                  .thumbnail.extension!,
                              comicList:
                                  state.model.characterList[index].comics,
                            ),
                          )),
                      child: CharacterCard(
                        character: state.model.characterList[index],
                      ),
                    );
                    // OpenContainerWrapper(
                    //   transitionType: transitionType,
                    //   openBuilder: (context, closedOpenContainer) =>
                    //     OpenContainerCharacterDetails(
                    //   character: state.model.characterList[index],
                    //   path: state.model.characterList[index].thumbnail.path!,
                    //   extension: state
                    //       .model.characterList[index].thumbnail.extension!,
                    //   comicList: state.model.characterList[index].comics,
                    // ),
                    //   closedBuilder: (context, openContainer) => CharacterCard(
                    //     character: state.model.characterList[index],
                    //   ),
                    // );
                  } else {
                    Timer(const Duration(milliseconds: 10), () {
                      _scrollController
                          .jumpTo(_scrollController.position.maxScrollExtent);
                    });
                    return const Center(
                      child: SizedBox(
                        width: 70,
                        height: 70,
                        child: BottomLoading(),
                      ),
                    );
                  }
                },
              );
            case CharacterStatus.failure:
              return const Center(
                key: Key('failed_to_fetch_characters'),
                child: Card(
                  child: ListTile(
                    title: Text('failed to fetch characters'),
                  ),
                ),
              );
            default:
              return const SizedBox.shrink();
          }
        }
        return const SizedBox.shrink();
      },
    );
  }
}
