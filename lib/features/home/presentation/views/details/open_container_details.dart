import 'package:flutter/material.dart';
import 'package:flutter_comics_app/features/home/domain/entities/entities.dart';
import 'package:flutter_comics_app/features/home/presentation/views/details/character_comic.dart';
import 'package:flutter_comics_app/features/home/presentation/views/details/widgets/appbar_home.dart';
import 'package:flutter_comics_app/features/home/presentation/widgets/hero_widget.dart';


class OpenContainerCharacterDetails extends StatelessWidget {
  const OpenContainerCharacterDetails({
    Key? key,
    required this.character,
    required this.path,
    required this.extension,
    required this.comicList,
  }) : super(key: key);
  final CharacterEntity character;
  final String path;
  final String extension;
  final List<String> comicList;
  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Scaffold(
      appBar: appbarHomeWidget(context),
      body: ListView(
        children: [
          Stack(
            children: [
              Container(
                height: size.height * 0.3,
                width: size.width,
                // color: Colors.black38,
                padding: const EdgeInsets.all(5.0),
                child: HeroWidget(
                  tag: character.name,
                  radius: 21.0,
                  assetImage: 'assets/images/no-image.jpg',
                  networkImage: "$path.$extension",
                  fit: BoxFit.fill,
                ),
              ),
              Positioned(
                  left: 12.0,
                  top: 6.0,
                  child: Text(
                    character.name,
                    style: const TextStyle(
                        fontSize: 21.0, fontWeight: FontWeight.bold),
                  ))
            ],
          ),
          const Padding(
            padding: EdgeInsets.symmetric(horizontal: 8.0),
            child: Text(
              'Lista de Comics asociados',
              style: TextStyle(fontSize: 18.0),
            ),
          ),
          CharacterComics(comicList: comicList)
        ],
      ),
    );
  }
}
