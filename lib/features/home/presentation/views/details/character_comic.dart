import 'package:flutter/material.dart';
// import 'package:flutter_comics_app/features/presentation/views/details/widgets/appbar.dart';

class CharacterComics extends StatelessWidget {
  const CharacterComics({
    Key? key,
    required this.comicList,
  }) : super(key: key);
  final List<String> comicList;
  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Container(
      height: size.height * 0.5,
      padding: const EdgeInsets.only(top: 12.0),
      child: ListView.builder(
        itemCount: comicList.length,
        itemBuilder: (context, int index) {
          final character = comicList[index];
          return Padding(
            padding: const EdgeInsets.symmetric(
              vertical: 8.0,
              horizontal: 4.0,
            ),
            child: ListTile(
              leading: const CircleAvatar(
                radius: 21.0,
                backgroundColor: Colors.blue,
              ),
              title: Text(character),
            ),
          );
        },
      ),
    );
  }
}
