part of 'character_bloc.dart';

abstract class CharacterState extends Equatable {
  const CharacterState(this.model);
  final Model model;

  @override
  List<Object> get props => [];
}

class CharacterInitialState extends CharacterState {
  const CharacterInitialState(Model model) : super(model);
}

class SearchCharacterByNameUploadedState extends CharacterState {
  const SearchCharacterByNameUploadedState(Model model) : super(model);
}

class CharacterUploadState extends CharacterState {
  const CharacterUploadState(Model model) : super(model);
}

class SearchCharacterByNameErrorUploadState extends CharacterState {
  const SearchCharacterByNameErrorUploadState(Model model) : super(model);
}

class CharacterDBUploadedState extends CharacterState {
  const CharacterDBUploadedState(Model model) : super(model);
}

class AddedCharacterDBState extends CharacterState {
  const AddedCharacterDBState(Model model) : super(model);
}

class CharactersUploadedState extends CharacterState {
  const CharactersUploadedState(Model model) : super(model);
}

class CharactersUploadErrorState extends CharacterState {
  const CharactersUploadErrorState(Model model) : super(model);
}

class Model extends Equatable {
  final List<CharacterEntity> characterSearchList;
  final List<CharacterEntity> characterList;
  final CharacterStatus status;
  final bool hasReachedMax;
  final String query;
  const Model({
    this.query = '',
    this.hasReachedMax = false,
    this.status = CharacterStatus.initial,
    this.characterList = const <CharacterEntity>[],
    this.characterSearchList = const <CharacterEntity>[],
  });

  Model copyWith({
    String? query,
    bool? hasReachedMax,
    CharacterStatus? status,
    List<CharacterEntity>? characterList,
    List<CharacterEntity>? characterSearchList,
  }) =>
      Model(
        query: query ?? this.query,
        status: status ?? this.status,
        hasReachedMax: hasReachedMax ?? this.hasReachedMax,
        characterList: characterList ?? this.characterList,
        characterSearchList: characterSearchList ?? this.characterSearchList,
      );

  @override
  List<Object?> get props =>
      [status, characterSearchList, hasReachedMax, query, characterList];
}

enum CharacterStatus { initial, success, failure }
