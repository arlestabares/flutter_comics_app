import 'package:bloc/bloc.dart';
import 'package:bloc_concurrency/bloc_concurrency.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter_comics_app/features/home/domain/entities/entities.dart';
import 'package:stream_transform/stream_transform.dart';

import '../../domain/usecases/use_cases.dart';

part 'character_event.dart';
part 'character_state.dart';

const throttleDuration = Duration(milliseconds: 100);

EventTransformer<E> throttleDroppable<E>(Duration duration) {
  return (events, mapper) {
    return droppable<E>().call(events.throttle(duration), mapper);
  };
}

class CharacterBloc extends Bloc<CharacterEvent, CharacterState> {
  final GetFavoritesCharacterUseCase getDetailsCharacter;
  final GetCharacterUseCase getCharacterUseCase;
  CharacterBloc({
    required GetCharacterUseCase characterUseCase,
    required GetFavoritesCharacterUseCase characterDetailsUseCase,
  })  : getDetailsCharacter = characterDetailsUseCase,
        getCharacterUseCase = characterUseCase,
        super(initialState) {
    on<CharacterFetchedEvent>(_onCharactersUploadEvent,
        transformer: throttleDroppable(throttleDuration));
  }
  static CharacterState get initialState =>
      const CharacterInitialState(Model());

  _onCharactersUploadEvent(
    CharacterFetchedEvent event,
    Emitter<CharacterState> emit,
  ) async {
    if (state.model.hasReachedMax) return;
    var currentCharactersContent = <CharacterEntity>[];
    if (state is CharactersUploadedState) {
      currentCharactersContent = state.model.characterList;
    }
    final result = await getCharacterUseCase
        .call(ParamsOffset(state.model.characterList.length));
    result.fold(
      (failure) => emit(CharactersUploadErrorState(
        state.model.copyWith(status: CharacterStatus.failure),
      )),
      (data) {
        if (data.results.isEmpty) {
          emit(CharactersUploadedState(
            state.model.copyWith(
              status: CharacterStatus.success,
              hasReachedMax: true,
            ),
          ));
        } else {
          emit(
            CharactersUploadedState(
              state.model.copyWith(
                status: CharacterStatus.success,
                characterList: currentCharactersContent..addAll(data.results),
                hasReachedMax: false,
              ),
            ),
          );
        }
      },
    );
  }
}
