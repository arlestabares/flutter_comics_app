part of 'character_bloc.dart';

abstract class CharacterEvent extends Equatable {
  const CharacterEvent();

  @override
  List<Object> get props => [];
}

class SearchCharacterByNameEvent extends CharacterEvent {
  final String nameCharacter;

  const SearchCharacterByNameEvent(this.nameCharacter);
}

class CharacterFetchedEvent extends CharacterEvent {}

class CharactersDBUploadedEvent extends CharacterEvent {}

class AddedCharacterDBEvent extends CharacterEvent {}

