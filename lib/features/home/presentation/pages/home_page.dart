import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_comics_app/features/home/presentation/views/home/character_view.dart';
import 'package:flutter_comics_app/features/search/presentation/bloc/search_bloc.dart';
import 'package:flutter_comics_app/features/search/presentation/views/search_delegate/search_delegate.dart';


class HomePage extends StatelessWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // late TextEditingController controller = TextEditingController();
    
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: const Text('Characters'),
        actions: <Widget>[
          IconButton(
            icon: const Icon(Icons.favorite_border_sharp),
            onPressed: (){  
              context.read<SearchBloc>().add(GetFavoritesFromDbEvent());
              Navigator.pushNamed(context, 'favorites_page');
            },
          ),
          IconButton(
            icon: const Icon(Icons.search),
            onPressed: () {
              // Navigator.pushNamed(context, 'search_page');
              showSearch(
                context: context,
                delegate: CharactersSearchDelegate(),
              );
            },
          )
        ],
      ),
      body: const CharactersView(),
    );
  }
}

