import 'package:flutter/material.dart';

class InkWellOverlay extends StatelessWidget {
  final VoidCallback? openContainer;
  final double? height;
  final double? width;
  final Widget? child;

  const InkWellOverlay({
    Key? key,
    this.openContainer,
    this.height,
    this.width,
    this.child,
  }):super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: height,
      width: width,
      child: InkWell(
        onTap: openContainer,
        child: child,
      ),
    );
  }
}
