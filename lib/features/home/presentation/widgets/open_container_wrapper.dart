import 'package:animations/animations.dart';
import 'package:flutter/material.dart';

class OpenContainerWrapper extends StatelessWidget {
  final CloseContainerBuilder? closedBuilder;
  final ContainerTransitionType? transitionType;
  final OpenContainerBuilder? openBuilder;

  const OpenContainerWrapper({
    Key? key,
    required this.closedBuilder,
    required this.transitionType,
    required this.openBuilder,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    return OpenContainer(
        transitionType: transitionType!,
        openBuilder: openBuilder!,
        closedBuilder: closedBuilder!,
        openColor: theme.cardColor,
        closedColor: theme.cardColor,
        closedElevation: 0,
        closedShape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(0)),
        )
        // onClosed: onClosed!,
        );
  }
}
