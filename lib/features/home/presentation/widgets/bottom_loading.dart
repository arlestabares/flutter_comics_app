import 'package:flutter/material.dart';

class BottomLoading extends StatelessWidget {
  const BottomLoading({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      key: const Key('bottom_loading'),
      alignment: Alignment.center,
      child: const Center(
          child: Image(
        width: 100.0,
        height: 100.0,
        fit: BoxFit.cover,
        image: AssetImage(
          'assets/images/marvel_loader.gif',
        ),
      )),
    );
  }
}
