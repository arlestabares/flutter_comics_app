import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_comics_app/features/home/presentation/bloc/character_bloc.dart';
import 'package:flutter_comics_app/features/search/domain/usecases/get_all_character_db.dart';
import 'package:flutter_comics_app/features/search/domain/usecases/insert_character_db.dart';
import 'package:flutter_comics_app/features/search/domain/usecases/use_cases.dart';
import 'package:flutter_comics_app/features/search/presentation/bloc/search_bloc.dart';
import 'package:flutter_comics_app/injector_container.dart';

import '../home/domain/usecases/use_cases.dart';

class MultiBlocProviderWidget extends StatelessWidget {
  const MultiBlocProviderWidget({Key? key, required this.child})
      : super(key: key);
  final Widget child;
  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider<CharacterBloc>(
          create: (context) => CharacterBloc(
            characterUseCase: sl.get<GetCharacterUseCase>(),
            characterDetailsUseCase: sl.get<GetFavoritesCharacterUseCase>(),
          )..add(CharacterFetchedEvent()),
        ),
        BlocProvider<SearchBloc>(
          create: (context) => SearchBloc(
            findAllCharacterDb: sl.get<GetAllCharacterDbUseCase>(),
            insertCharacterSearchDbUseCase: sl.get<InsertCharacterDbUseCase>(),
            searchCharacterByNameUseCase:
                sl.get<GetSearchCharacterByNameUseCase>(),
            searchCharacterDbUseCase: sl.get<GetSearchCharacterDbUseCase>(),
          ),
        ),
      ],
      child: child,
    );
  }
}
