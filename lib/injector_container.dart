import 'package:flutter_comics_app/features/home/data/datasources/datasource.dart';
import 'package:flutter_comics_app/features/home/data/repositories/character_repository.dart';
import 'package:flutter_comics_app/features/home/domain/repositories/character_repository.dart';
import 'package:flutter_comics_app/features/home/domain/usecases/use_cases.dart';
import 'package:flutter_comics_app/features/home/presentation/bloc/character_bloc.dart';
import 'package:flutter_comics_app/features/search/data/datasources/remote/remote_data_source.dart';
import 'package:flutter_comics_app/features/search/data/repositories/character_repository.dart';
import 'package:flutter_comics_app/features/search/data/repositories/sqlite_repository.dart';
import 'package:flutter_comics_app/features/search/domain/repositories/character_search_repository.dart';
import 'package:flutter_comics_app/features/search/domain/repositories/sqlite_domain_repository.dart';
import 'package:flutter_comics_app/features/search/domain/usecases/get_all_character_db.dart';
import 'package:flutter_comics_app/features/search/domain/usecases/insert_character_db.dart';
import 'package:flutter_comics_app/features/search/domain/usecases/use_cases.dart';
import 'package:flutter_comics_app/features/search/presentation/bloc/search_bloc.dart';
import 'package:get_it/get_it.dart';

final sl = GetIt.instance;
Future<void> init() async {
  
  //CharacterBloc
  sl.registerFactory(
    () => CharacterBloc(
      characterUseCase: sl(),
      characterDetailsUseCase: sl(),
    ),
  );
  //Feature Search
  sl.registerFactory(
    () => SearchBloc(
      findAllCharacterDb:sl() ,
      insertCharacterSearchDbUseCase: sl(),
      searchCharacterDbUseCase: sl(),
      searchCharacterByNameUseCase: sl(),
    ),
  );
  sl.registerLazySingleton(() => GetFavoritesCharacterUseCase(sl()));
  sl.registerLazySingleton(() => GetCharacterDBUseCase(sl()));
  sl.registerLazySingleton(() => GetCharacterUseCase(sl()));
  sl.registerLazySingleton(() => GetAllCharacterDbUseCase(sl()));
  
  //usecase db
  sl.registerLazySingleton(() => InsertCharacterDbUseCase(sl()));
  //Registro SqliteRepository
  sl.registerLazySingleton<SqliteDomainRepository>(() => SqliteRepository());

  //Registro de Repositorio
  sl.registerLazySingleton<CharacterDomainRepository>(
      () => CharacterRepostoryImpl(sl()));
  //Datasources
  sl.registerLazySingleton<IRemoteDataSource>(() => RemoteDataSourceImpl());
  //External
  //
  //Feature Search usecase
  sl.registerLazySingleton(() => GetSearchCharacterByNameUseCase(sl()));
  sl.registerLazySingleton(() => GetSearchCharacterDbUseCase(sl()));
  //repository of Domain
  sl.registerLazySingleton<CharacterSearchDomainRepository>(
      () => CharacterSearchRepostoryImpl(sl()));
  //Repository
  sl.registerLazySingleton<IRemoteSearchDataSource>(
      () => RemoteSearchDataSourceImpl());

  //Core
}
