import 'package:flutter/material.dart';
import 'package:flutter_comics_app/features/home/presentation/pages/home_page.dart';
import 'package:flutter_comics_app/features/home/presentation/views/home/character_view.dart';
import 'package:flutter_comics_app/features/search/presentation/pages/favorites_page.dart';

final Map<String, Widget Function(BuildContext)> appRoutes = {
  'home_page': (_) => const HomePage(),
  'character_home_view': (_) => const CharactersView(),
  'favorites_page': (_) => const FavoritesPage(),
};
