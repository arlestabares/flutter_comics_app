import 'package:flutter/material.dart';
import 'package:flutter_comics_app/features/blocs/multi_bloc_provider.dart';
import 'package:flutter_comics_app/injector_container.dart' as di;
import 'package:flutter_comics_app/routes/routes.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await di.init();
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MultiBlocProviderWidget(
      child: MaterialApp(
        theme: ThemeData(useMaterial3: true),
        title: 'Material App',
        initialRoute: 'home_page',
        // initialRoute: 'character_home_view',
        routes: appRoutes,
      ),
    );
  }
}
