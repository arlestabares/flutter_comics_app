class UrlPaths {
  UrlPaths._constructorPrivate();
  static const String baseUrl = 'https://gateway.marvel.com/v1/public';
  static const String timeStamp = '1';
  // static const String apiKey = 'ddb47bf68c1b7df0dd01d3daa3b846f9';
  // static const String hash = '0b3e7c72004dd70af48b20850b003f3c';
  static const String apiKey = '373d813f41f6c5a96402b6f45a6f9ef1';//adtflutter
  static const String hash = '316402611d024e98d592ac2c1b8b964d';//adtflutter
  // static const String apiKey = 'e84610d6d97db0f0a28a9263fcfb003a';
  // static const String hash = '6585fe5187cea41c28056c67e17ce293';
  static String searchCharactersByName(int limit, String name, int offset) =>
      '$baseUrl/characters?limit=$limit&nameStartsWith=$name&ts=$timeStamp&offset=$offset&apikey=$apiKey&hash=$hash';
  static String searchCharacters(int limit,  int offset) =>
      '$baseUrl/characters?limit=$limit&ts=$timeStamp&offset=$offset&apikey=$apiKey&hash=$hash';


}
