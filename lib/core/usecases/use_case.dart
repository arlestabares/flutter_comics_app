import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter_comics_app/core/errors/failure.dart';

abstract class IUseCaseCore<Type, Params> {
  Future<Either<Failure, Type>> call(Params param);
}

class NoParams extends Equatable {
  @override
  List<Object?> get props => [];
}
