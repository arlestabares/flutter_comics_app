import 'package:flutter_comics_app/core/app/data/mixins/domain_converter.dart';
import 'package:flutter_comics_app/features/search/domain/entities/sqlite_entity.dart';

class CharacterSearchDTO with DomainConverter<SqliteEntity> {
  final int? id;
  final String? name;
  final String? description;
  final String? thumbnail;
  final List<String>? comics;
  final List<String>? series;
  final List<String>? stories;
  final List<String>? events;

  CharacterSearchDTO({
    this.id,
    this.name,
    this.description,
    this.thumbnail,
    this.comics,
    this.series,
    this.stories,
    this.events,
  });
  factory CharacterSearchDTO.fromJsom(Map<String, dynamic> json) {
    return CharacterSearchDTO(
      id: json['id'],
      name: json['name'],
      description: json['description'],
      thumbnail: json['thumbnail'] ?? '',
      comics: (json["comics"]).map((e) => e as String),
      series: (json["series"] as List).map((e) => e as String).toList(),
      stories: (json["stories"] as List).map((e) => e as String).toList(),
      events: (json["events"] as List).map((e) => e as String).toList(),
      // comics: List<String>.from(json["comics"].map((x) => x)),
     
    );
  }
  Map<String, dynamic> toJson() => {
        'id': id,
        'name': name,
        'thumbnail': thumbnail,
        'description': description,
        'comics': (comics!.map((x) => x)).toList(),
        'series': (series!.map((x) => x)).toList(),
        'stories': (stories!.map((x) => x)).toList(),
        'events': (events!.map((x) => x)).toList(),
      };

  @override
  SqliteEntity toDomain() => SqliteEntity(
        id: id ?? 0,
        name: name ?? '',
        thumbnail: thumbnail ?? '',
        description: description ?? '',
        comics: comics ?? [],
        series: series ?? [],
        stories: stories ?? [],
        events: events ?? [],
      );

  factory CharacterSearchDTO.fromDomainToDto(SqliteEntity entity) =>
      CharacterSearchDTO(
        id: entity.id,
        name: entity.name,
        thumbnail: entity.thumbnail,
        description: entity.description,
        comics: entity.comics,
        series: entity.series,
        stories: entity.stories,
        events: entity.events,
      );
}
