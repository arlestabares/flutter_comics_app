import 'package:flutter_comics_app/core/app/data/datasources/local/dto/character_search_picture.dart';
import 'package:path_provider/path_provider.dart';
import 'package:sqlbrite/sqlbrite.dart';
import 'package:path/path.dart' as p;
import 'package:synchronized/synchronized.dart';

class DatabaseHelper {
  static const _databaseName = 'MyCharacter.db';
  static const _databaseVersion = 1;

  static const characterTable = 'Character';
  static const characterId = 'characterId';

  static late BriteDatabase _streamDatabase;

  DatabaseHelper._privateConstructor();

  static final DatabaseHelper instance = DatabaseHelper._privateConstructor();
  static var lock = Lock();

  static Database? _database;

  Future _onCreate(Database db, int version) async {
    await db.execute('''
        CREATE TABLE IF NOT EXISTS $characterTable (
          id INTEGER PRIMARY KEY,
          name TEXT,
          description TEXT,
          thumbnail TEXT,
          comics TEXT,
          series TEXT,
          stories TEXT,
          events TEXT)
        ''');
  }

  // abre la base de datos (y la crea si no existe)
  Future<Database> _initDatabase() async {
    final documentsDirectory = await getApplicationDocumentsDirectory();
    final path = p.join(documentsDirectory.path, _databaseName);
    Sqflite.setDebugModeOn(true);
    return openDatabase(path, version: _databaseVersion, onCreate: _onCreate);
  }

  Future<Database> get database async {
    if (_database != null) return _database!;
    // Objeto que evita el acceso simultáneo a los datos
    await lock.synchronized(
      () async {
        //crea una instancia perezosa de la base de datos la primera vez
        //que se accede a ella
        if (_database == null) {
          _database = await _initDatabase();
          _streamDatabase = BriteDatabase(_database!);
        }
      },
    );
    return _database!;
  }

  Future<BriteDatabase> get streamDatabase async {
    await database;
    return _streamDatabase;
  }

  Future<List<CharacterSearchDTO>> findAllCharacterSearch() async {
    final db = await instance.streamDatabase;
    final characterList = await db.query(characterTable);
    final recipes = parseCharacterSearch(characterList);
    return recipes;
  }

  Stream<List<CharacterSearchDTO>> watchAllCharacters() async* {
    final db = await instance.streamDatabase;
    yield* db
        .createQuery(characterTable)
        .mapToList((row) => CharacterSearchDTO.fromJsom(row));
  }

  Future<CharacterSearchDTO> findCharacterById(int id) async {
    final db = await instance.streamDatabase;
    final charactersList = await db.query(characterTable, where: 'id = $id');
    final character = parseCharacterSearch(charactersList);
    return character.first;
  }

  List<CharacterSearchDTO> parseCharacterSearch(
      List<Map<String, Object?>> ingredientList) {
    final characters = <CharacterSearchDTO>[];
    for (var characterMap in ingredientList) {
      final character = CharacterSearchDTO.fromJsom(characterMap);
      characters.add(character);
    }
    return characters;
  }

  Future<int> insertCharacterSearch(CharacterSearchDTO character) {
    return _insert(characterTable, character.toJson());
  }

  Future<int> _insert(String table, Map<String, Object?> row) async {
    final db = await instance.streamDatabase;
    return db.insert(table, row, conflictAlgorithm: ConflictAlgorithm.replace);
  }

  Future<int> _delete(String table) async {
    final db = await instance.streamDatabase;
    return db.delete(table);
  }

  Future<int> deleteCharacterSearch(CharacterSearchDTO character) async {
    if (character.id != null) {
      return _delete(characterTable);
    } else {
      return Future.value(-1);
    }
  }

  Future<void> deleteCharacterSearchList(
      List<CharacterSearchDTO> charactersList) {
    for (var character in charactersList) {
      if (character.id != null) {
        _delete(characterTable);
      }
    }
    return Future.value();
  }

  void close() {
    _streamDatabase.close();
  }
}
