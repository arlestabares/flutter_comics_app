import 'package:equatable/equatable.dart';
import 'package:flutter_comics_app/core/app/data/models/models.dart';
import 'package:flutter_comics_app/features/home/domain/entities/entities.dart';
import 'package:flutter_comics_app/features/search/domain/entities/entities_search.dart';
// import 'package:flutter_comics_app/core/app/data/models/models.dart';
// import 'package:flutter_comics_app/features/home/domain/entities/entities.dart';




class DataModel extends Equatable {
  const DataModel({
    required this.offset,
    required this.limit,
    required this.total,
    required this.count,
    required this.results,
  });

  final int offset;
  final int limit;
  final int total;
  final int count;
  final List<CharacterModel> results;

  factory DataModel.fromJson(Map<String, dynamic> json) => DataModel(
        offset: json['offset'] ?? 0,
        limit: json['limit'] ?? 0,
        total: json['total'] ?? 0,
        count: json['count'] ?? 0,
        results: (json['results'] as List)
            .map((e) => CharacterModel.fromJson(e))
            .toList(),
        // List<ComicsModel>.from(
        //   json["results"].map((x) => ComicsModel.fromJson(x)),
      );

  Data dataModeltoEntity() => Data(
        offset: offset,
        limit: limit,
        total: total,
        count: count,
        results: results.map((e) => e.characterEntityModeltoEntity()).toList(),
      );
  DataSearchEntity dataSearchModeltoEntity() => DataSearchEntity(
        offset: offset,
        limit: limit,
        total: total,
        count: count,
        results: results.map((e) => e.characterSearchModeltoEntity()).toList(),
      );

  @override
  List<Object?> get props => [
        offset,
        limit,
        total,
        count,
        results,
      ];
}
