class ComicsItemModel {
  ComicsItemModel({
    this.resourceUri,
    this.name,
  });

  String? resourceUri;
  String? name;

  factory ComicsItemModel.fromJson(Map<String, dynamic> json) => ComicsItemModel(
        resourceUri: json["resourceURI"],
        name: json["name"],
      );
}