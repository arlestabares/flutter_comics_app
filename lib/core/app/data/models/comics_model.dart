
import 'package:flutter_comics_app/core/app/data/models/models.dart';

class ComicsModel {
  ComicsModel({
    this.available,
    this.collectionUri,
    this.items,
    this.returned,
  });

  int? available;
  String? collectionUri;
  List<ComicsItemModel>? items;
  int? returned;

  ComicsModel.fromJson(Map<String, dynamic> json) {
    available = json["available"];
    collectionUri = json["collectionURI"];
    if (json['items'] != null) {
      json['items'].forEach((v) {
        items!.add(ComicsItemModel.fromJson(v));
      });
    }

    returned = json["returned"];
  }
}
