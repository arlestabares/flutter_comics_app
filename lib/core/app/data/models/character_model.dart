import 'package:equatable/equatable.dart';
import 'package:flutter_comics_app/core/app/data/models/models.dart';

import '../../../../features/home/domain/entities/character.dart';
import '../../../../features/search/domain/entities/entities_search.dart';

class CharacterModel extends Equatable {
  const CharacterModel({
    required this.id,
    required this.name,
    required this.description,
    required this.thumbnail,
    required this.comics,
    required this.series,
    required this.stories,
    required this.events,
  });

  final int id;
  final String name;
  final String description;
  final Thumbnail thumbnail;
  final List<String> comics;
  final List<String> series;
  final List<String> stories;
  final List<String> events;
  factory CharacterModel.fromJson(Map<String, dynamic> json) => CharacterModel(
        id: json['id'],
        name: json['name'],
        description: json['description'],
        thumbnail: Thumbnail.fromJson(json["thumbnail"]),
        comics: (json['comics']['items'] as List)
            .map((comic) => comic['name'] as String)
            .toList(),
        series: (json['series']['items'] as List)
            .map((comic) => comic['name'] as String)
            .toList(),
        stories: (json['stories']['items'] as List)
            .map((comic) => comic['name'] as String)
            .toList(),
        events: (json['events']['items'] as List)
            .map((comic) => comic['name'] as String)
            .toList(),
      );
  Map<String, dynamic> toJson() => {
        'id': id,
        'name': name,
        'description': description,
        'thumbnail': thumbnail,
        'comics': List<dynamic>.from(comics.map((x) => x)),
        'stories': List<dynamic>.from(stories.map((x) => x)),
        'events': List<dynamic>.from(events.map((x) => x))
      };

  CharacterEntity characterEntityModeltoEntity() => CharacterEntity(
        id: id,
        name: name,
        description: description,
        thumbnail: thumbnail,
        comics: comics,
        series: series,
        stories: stories,
        events: events,
      );
  CharacterSearchEntity characterSearchModeltoEntity() => CharacterSearchEntity(
        id: id,
        name: name,
        description: description,
        thumbnail: thumbnail,
        comics: comics,
        series: series,
        stories: stories,
        events: events,
      );

  @override
  List<Object?> get props => [
        id,
        name,
        description,
        thumbnail,
        comics,
        series,
        stories,
        events,
      ];
}
